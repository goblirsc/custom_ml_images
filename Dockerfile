# Select a base image from which to extend
FROM registry.cern.ch/ml/kf-14-tensorflow-jupyter:v4

USER root
ENV MAMBA_USER=jovyan
 
# Install required packages
COPY requirements.txt /requirements.txt

RUN apt-get -qq update && pip3 install -r /requirements.txt


USER jovyan
WORKDIR /home/jovyan 
ENV MAMBA_ROOT_PREFIX=/home/jovyan/conda/
ENV MAMBA_TARGET_PREFIX=/home/jovyan/conda/

COPY env.yaml /home/jovyan/env.yaml
RUN curl micro.mamba.pm/install.sh | bash
RUN source ~/.bashrc \
&& micromamba shell init --shell=bash --prefix=~/micromamba \
&& micromamba install -y -n base -f /home/jovyan/env.yaml

ARG MAMBA_DOCKERFILE_ACTIVATE=1

# The following line is mandatory:
CMD ["sh", "-c", \
     "source ~/.bashrc && micromamba activate base && jupyter lab --notebook-dir=/home/jovyan --ip=0.0.0.0 --no-browser \
      --allow-root --port=8888 --LabApp.token='' --LabApp.password='' \
      --LabApp.allow_origin='*' --LabApp.base_url=${NB_PREFIX}"]

